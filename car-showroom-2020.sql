-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Окт 14 2020 г., 23:28
-- Версия сервера: 10.3.22-MariaDB
-- Версия PHP: 7.3.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `car-showroom-2020`
--

-- --------------------------------------------------------

--
-- Структура таблицы `basket`
--

CREATE TABLE `basket` (
  `id` int(11) NOT NULL COMMENT 'Идентификатор',
  `create_dt` datetime NOT NULL DEFAULT curdate() COMMENT 'Дата создания записи',
  `update_dt` datetime NOT NULL DEFAULT curdate() COMMENT 'Дата обновления записи',
  `product_id` int(11) NOT NULL COMMENT 'Идентификатор атомобиля',
  `delivery` int(11) NOT NULL DEFAULT 0 COMMENT 'Доставка',
  `payment_id` int(11) DEFAULT NULL COMMENT 'Вид оплаты',
  `client_id` int(11) NOT NULL COMMENT 'Идентификатор клиента',
  `basket_status_id` int(11) NOT NULL COMMENT 'Статус заказа'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Корзина покупателя';

--
-- Дамп данных таблицы `basket`
--

INSERT INTO `basket` (`id`, `create_dt`, `update_dt`, `product_id`, `delivery`, `payment_id`, `client_id`, `basket_status_id`) VALUES
(1, '2020-10-14 00:00:00', '2020-10-14 00:00:00', 2, 0, 2, 3, 1),
(2, '2020-10-14 00:00:00', '2020-10-14 00:00:00', 2, 0, 2, 2, 1),
(3, '2020-10-14 00:00:00', '2020-10-14 00:00:00', 11, 0, 1, 1, 1),
(4, '2020-10-14 00:00:00', '2020-10-14 00:00:00', 3, 1, 1, 12, 1),
(5, '2020-10-14 00:00:00', '2020-10-14 00:00:00', 3, 1, 1, 12, 1),
(6, '2020-10-14 00:00:00', '2020-10-14 00:00:00', 5, 1, 3, 5, 3),
(7, '2020-10-14 00:00:00', '2020-10-14 00:00:00', 4, 1, 3, 6, 3),
(8, '2020-10-14 00:00:00', '2020-10-14 00:00:00', 5, 0, 1, 7, 3),
(9, '2020-10-14 00:00:00', '2020-10-14 00:00:00', 3, 1, 2, 9, 2),
(10, '2020-10-14 00:00:00', '2020-10-14 00:00:00', 6, 1, 2, 10, 2),
(11, '2020-10-14 00:00:00', '2020-10-14 00:00:00', 2, 1, 2, 2, 1),
(12, '2020-10-14 00:00:00', '2020-10-14 00:00:00', 2, 1, 2, 1, 1),
(13, '2020-10-14 00:00:00', '2020-10-14 00:00:00', 2, 0, 2, 3, 1),
(14, '2020-10-14 00:00:00', '2020-10-14 00:00:00', 2, 0, 2, 5, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `basket_status`
--

CREATE TABLE `basket_status` (
  `id` int(11) NOT NULL COMMENT 'Идентификатор',
  `create_dt` datetime NOT NULL DEFAULT curdate() COMMENT 'Дата создания записи',
  `update_dt` datetime NOT NULL DEFAULT curdate() COMMENT 'Дата обновления записи',
  `name` varchar(100) NOT NULL COMMENT 'Наименование статуса'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Статус заказа';

--
-- Дамп данных таблицы `basket_status`
--

INSERT INTO `basket_status` (`id`, `create_dt`, `update_dt`, `name`) VALUES
(1, '2020-10-14 00:00:00', '2020-10-14 00:00:00', 'Выполнено'),
(2, '2020-10-14 00:00:00', '2020-10-14 00:00:00', 'В работе'),
(3, '2020-10-14 00:00:00', '2020-10-14 00:00:00', 'Ожидание оплаты');

-- --------------------------------------------------------

--
-- Структура таблицы `brand`
--

CREATE TABLE `brand` (
  `id` int(11) NOT NULL COMMENT 'Идентификатор',
  `create_dt` datetime NOT NULL DEFAULT curdate() COMMENT 'Дата создания записи',
  `update_dt` datetime NOT NULL DEFAULT curdate() COMMENT 'Дата обновления записи',
  `name` varchar(100) NOT NULL COMMENT 'Наименование марки',
  `country_id` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Марка автомобиля';

--
-- Дамп данных таблицы `brand`
--

INSERT INTO `brand` (`id`, `create_dt`, `update_dt`, `name`, `country_id`) VALUES
(1, '2020-10-14 00:00:00', '2020-10-14 00:00:00', 'LADA', 2),
(2, '2020-10-14 00:00:00', '2020-10-14 00:00:00', 'Mercedes', 6),
(3, '2020-10-14 00:00:00', '2020-10-14 00:00:00', 'Volkswagen', 3),
(4, '2020-10-14 00:00:00', '2020-10-14 00:00:00', 'Skoda', 5),
(5, '2020-10-14 00:00:00', '2020-10-14 00:00:00', 'Audi', 3),
(6, '2020-10-14 00:00:00', '2020-10-14 00:00:00', 'Mercedes-Benz', 6),
(7, '2020-10-14 00:00:00', '2020-10-14 00:00:00', 'Daewoo', 9),
(8, '2020-10-14 00:00:00', '2020-10-14 00:00:00', 'Toyota', 10),
(9, '2020-10-14 00:00:00', '2020-10-14 00:00:00', 'Mitsubishi', 10),
(10, '2020-10-14 00:00:00', '2020-10-14 00:00:00', 'Honda', 10),
(11, '2020-10-14 00:00:00', '2020-10-14 00:00:00', 'Kia', 9);

-- --------------------------------------------------------

--
-- Структура таблицы `client`
--

CREATE TABLE `client` (
  `id` int(11) NOT NULL COMMENT 'Идентификатор',
  `create_dt` date NOT NULL DEFAULT curdate() COMMENT 'Дата создания записи',
  `update_dt` datetime NOT NULL DEFAULT curdate() COMMENT 'Дата обновления записи',
  `name_1` varchar(150) NOT NULL COMMENT 'Имя клиента',
  `name_2` varchar(150) NOT NULL COMMENT 'Фамилия клиента',
  `name_3` varchar(150) NOT NULL COMMENT 'Отчество клиента',
  `passport` varchar(100) NOT NULL COMMENT 'Паспортные данные',
  `address` varchar(300) DEFAULT NULL COMMENT 'Домашний адрес',
  `phone` int(11) NOT NULL COMMENT 'Телефон',
  `user_id` int(11) NOT NULL COMMENT 'Идентификатор пользователя системы'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Карточки клиентов';

--
-- Дамп данных таблицы `client`
--

INSERT INTO `client` (`id`, `create_dt`, `update_dt`, `name_1`, `name_2`, `name_3`, `passport`, `address`, `phone`, `user_id`) VALUES
(1, '2020-10-14', '2020-10-14 00:00:00', 'Иванов', 'Сегрей', 'Сергеевич', '1234568912', 'Московская улица, 27\r\nПенза, Россия, 440000', 958366562, 4),
(2, '2020-10-14', '2020-10-14 00:00:00', 'Краснова', 'Алина', 'Владимировна', '2341236789', 'улица Максима Горького, 18к1\r\nПенза, Россия, 440000', 956255845, 5),
(3, '2020-10-14', '2020-10-14 00:00:00', 'Петров', 'Виктор', 'Игоревич', '1234568912', 'улица Кирова, 54\r\nПенза, Россия, 440000', 325699852, 6),
(4, '2020-10-14', '2020-10-14 00:00:00', 'Винокурова', 'Алина', 'Владимирович', '1234568912', 'улица Володарского, 45/38\r\nПенза, Россия, 440000', 654896359, 7),
(5, '2020-10-14', '2020-10-14 00:00:00', 'Кастромитина', 'Ирина', 'Петровна', '1234568912', 'улица Славы, 10А\r\nПенза, Россия', 896523658, 8),
(6, '2020-10-14', '2020-10-14 00:00:00', 'Героев', 'Владимир', 'Игоревич', '2341236789', 'улица Славы, 7\r\nПенза, Россия, 440000', 874965236, 9),
(7, '2020-10-14', '2020-10-14 00:00:00', 'Игрушкин', 'Сегрей', 'Николаев', '1234568912', 'улица Куприна, 1\r\nПенза, Россия, 440000', 789654123, 10),
(8, '2020-10-14', '2020-10-14 00:00:00', 'Земской', 'Игорь', 'Валерьевич', '1234568912', 'Театральный проезд, 1\r\nПенза, Россия, 440000', 741258952, 11),
(9, '2020-10-14', '2020-10-14 00:00:00', 'Петров', 'Роман', 'Александрович', '1234568912', 'улица Суворова, 20\r\nПенза, Россия, 440000', 789321564, 12),
(10, '2020-10-14', '2020-10-14 00:00:00', 'Ирушкин', 'Сегрей', 'Владимирович', '2341236789', 'Октябрьская улица, 3\r\nПенза, Россия, 440000', 852147657, 13),
(11, '2020-10-14', '2020-10-14 00:00:00', 'Земнухов', 'Виктор', 'Викторович', '1234568912', 'улица Володарского, 83\r\nПенза, Россия, 440000', 789321459, 14),
(12, '2020-10-14', '2020-10-14 00:00:00', 'Битов', 'Никита', 'Алексевич', '1234568912', 'улица Володарского, 71\r\nПенза, Россия, 440000', 951159852, 15);

-- --------------------------------------------------------

--
-- Структура таблицы `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL COMMENT 'Идентификатор',
  `create_dt` datetime NOT NULL DEFAULT curdate() COMMENT 'Дата создания записи',
  `update_dt` datetime NOT NULL DEFAULT curdate() COMMENT 'Дата обновления записи',
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Россия' COMMENT 'Наименование страны',
  `designation` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'RU' COMMENT 'Двузначное обозначение страны'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Страна изготовитель';

--
-- Дамп данных таблицы `country`
--

INSERT INTO `country` (`id`, `create_dt`, `update_dt`, `name`, `designation`) VALUES
(1, '2020-10-14 00:00:00', '2020-10-14 00:00:00', 'Неизвестно', 'XX'),
(2, '2020-10-14 00:00:00', '2020-10-14 00:00:00', 'Россия', 'RU'),
(3, '2020-10-14 00:00:00', '2020-10-14 00:00:00', 'Германия', 'DE'),
(4, '2020-10-14 00:00:00', '2020-10-14 00:00:00', 'Китай', 'CN'),
(5, '2020-10-14 00:00:00', '2020-10-14 00:00:00', 'Чехия', 'CZ'),
(6, '2020-10-14 00:00:00', '2020-10-14 00:00:00', 'США', 'US'),
(7, '2020-10-14 00:00:00', '2020-10-14 00:00:00', 'Франция', 'FR'),
(8, '2020-10-14 00:00:00', '2020-10-14 00:00:00', 'Англия', 'GB'),
(9, '2020-10-14 00:00:00', '2020-10-14 00:00:00', 'Республика Корея\r\n', 'KR'),
(10, '2020-10-14 00:00:00', '2020-10-14 00:00:00', 'Япония', 'JP');

-- --------------------------------------------------------

--
-- Структура таблицы `payment`
--

CREATE TABLE `payment` (
  `id` int(11) NOT NULL COMMENT 'Идентификатор',
  `create_dt` date NOT NULL DEFAULT curdate() COMMENT 'Дата создания записи',
  `update_dt` datetime NOT NULL DEFAULT curdate() COMMENT 'Дата обновления записи',
  `name` varchar(150) NOT NULL COMMENT 'Наименование оплаты'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Способы оплаты';

--
-- Дамп данных таблицы `payment`
--

INSERT INTO `payment` (`id`, `create_dt`, `update_dt`, `name`) VALUES
(1, '2020-10-12', '2020-10-12 00:00:00', 'Перечисление'),
(2, '2020-10-12', '2020-10-12 00:00:00', 'Наличные'),
(3, '2020-10-12', '2020-10-12 00:00:00', 'Кредит');

-- --------------------------------------------------------

--
-- Структура таблицы `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL COMMENT 'Идентификатор',
  `create_dt` date NOT NULL DEFAULT curdate() COMMENT 'Дата создания записи',
  `update_dt` datetime NOT NULL DEFAULT curdate() COMMENT 'Дата обновления записи',
  `model` varchar(150) NOT NULL COMMENT 'Модель автомобиля',
  `stock_availability` int(11) NOT NULL DEFAULT 0 COMMENT 'Наличие на складе',
  `stock_availability_dt` date DEFAULT curdate() COMMENT 'Когда будет на складе',
  `price` double NOT NULL DEFAULT 0 COMMENT 'Цена',
  `technical_details_id` int(11) NOT NULL COMMENT 'Идентификатор технических характеристик',
  `brand_id` int(11) NOT NULL COMMENT 'Марка автомобиля',
  `year` date NOT NULL DEFAULT '2020-01-01' COMMENT 'Дата выпуска'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Автомобили';

--
-- Дамп данных таблицы `product`
--

INSERT INTO `product` (`id`, `create_dt`, `update_dt`, `model`, `stock_availability`, `stock_availability_dt`, `price`, `technical_details_id`, `brand_id`, `year`) VALUES
(2, '2020-10-14', '2020-10-14 00:00:00', 'Granta I Рестайлинг', 5, '2020-10-14', 790000, 1, 1, '2020-01-01'),
(3, '2020-10-14', '2020-10-14 00:00:00', 'E-Класс V', 1, '2020-10-14', 2500000, 2, 6, '2020-01-01'),
(4, '2020-10-14', '2020-10-14 00:00:00', 'Passat B8', 5, '2020-10-14', 2000000, 3, 3, '2020-01-01'),
(5, '2020-10-14', '2020-10-14 00:00:00', 'Jetta VII', 1, '2020-10-14', 190000, 3, 3, '2020-01-01'),
(6, '2020-10-14', '2020-10-14 00:00:00', 'C-Класс IV', 0, '2020-12-10', 3000000, 4, 2, '2020-01-01'),
(7, '2020-10-14', '2020-10-14 00:00:00', 'Superb II', 2, '2020-10-14', 800000, 8, 4, '2020-01-01'),
(8, '2020-10-14', '2020-10-14 00:00:00', 'RS 3 Sportback I (8P)', 3, '2020-10-14', 678987, 8, 5, '2020-01-01'),
(9, '2020-10-14', '2020-10-14 00:00:00', 'Granta I Рестайлинг', 4, '2020-10-14', 567479, 9, 1, '2020-01-01'),
(10, '2020-10-14', '2020-10-14 00:00:00', 'Rapid I', 0, '2021-01-10', 2500000, 11, 4, '2020-01-01'),
(11, '2020-10-14', '2020-10-14 00:00:00', 'Golf II', 0, '2021-01-10', 657000, 7, 3, '2020-01-01'),
(12, '2020-10-14', '2020-10-14 00:00:00', 'Matiz I Рестайлинг', 2, '2020-10-14', 300000, 7, 7, '2020-01-01'),
(13, '2020-10-14', '2020-10-14 00:00:00', 'Matiz I Рестайлинг', 3, '2020-10-14', 290000, 6, 7, '2020-01-01');

-- --------------------------------------------------------

--
-- Структура таблицы `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL COMMENT 'Идентификатор',
  `create_dt` date NOT NULL DEFAULT curdate() COMMENT 'Дата создания записи',
  `update_dt` datetime NOT NULL DEFAULT curdate() COMMENT 'Дата обновления записи',
  `name` varchar(150) NOT NULL COMMENT 'Наименование роли'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Роль пользователя системы';

--
-- Дамп данных таблицы `role`
--

INSERT INTO `role` (`id`, `create_dt`, `update_dt`, `name`) VALUES
(1, '2020-10-14', '2020-10-14 00:00:00', 'Администратор'),
(2, '2020-10-14', '2020-10-14 00:00:00', 'Клиент');

-- --------------------------------------------------------

--
-- Структура таблицы `technical_details`
--

CREATE TABLE `technical_details` (
  `id` int(11) NOT NULL COMMENT 'Идентификатор',
  `create_dt` datetime NOT NULL DEFAULT curdate() COMMENT 'Дата создания записи',
  `update_dt` datetime NOT NULL DEFAULT curdate() COMMENT 'Дата обновления записи',
  `type` varchar(100) DEFAULT 'Седан' COMMENT 'Тип кузова',
  `count_doors` int(11) NOT NULL DEFAULT 4 COMMENT 'Количество дверей',
  `count_palce` int(11) NOT NULL DEFAULT 5 COMMENT 'Количество мест',
  `type_engine` varchar(100) DEFAULT 'ДВС' COMMENT 'Тип двигателя',
  `location_engine` varchar(100) DEFAULT 'Впереди' COMMENT 'Расположение двигателя',
  `volume_engine` double DEFAULT 1.6 COMMENT 'Рабочий объем двигателя'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Технические данные';

--
-- Дамп данных таблицы `technical_details`
--

INSERT INTO `technical_details` (`id`, `create_dt`, `update_dt`, `type`, `count_doors`, `count_palce`, `type_engine`, `location_engine`, `volume_engine`) VALUES
(1, '2020-10-14 00:00:00', '2020-10-14 00:00:00', 'Седан', 4, 5, 'ДВС', 'Впереди', 1.6),
(2, '2020-10-14 00:00:00', '2020-10-14 00:00:00', 'Седан', 4, 5, 'ДВС', 'Впереди', 1.8),
(3, '2020-10-14 00:00:00', '2020-10-14 00:00:00', 'Седан', 4, 5, 'ДВС', 'Впереди', 2),
(4, '2020-10-14 00:00:00', '2020-10-14 00:00:00', 'Седан', 4, 5, 'ДВС', 'Впереди', 2.2),
(5, '2020-10-14 00:00:00', '2020-10-14 00:00:00', 'Седан', 4, 5, 'ДВС', 'Впереди', 2.4),
(6, '2020-10-14 00:00:00', '2020-10-14 00:00:00', 'Хэтчбек', 5, 5, 'ДВС', 'Впереди', 1.6),
(7, '2020-10-14 00:00:00', '2020-10-14 00:00:00', 'Хэтчбек', 5, 5, 'ДВС', 'Впереди', 1.8),
(8, '2020-10-14 00:00:00', '2020-10-14 00:00:00', 'Хэтчбек', 5, 5, 'ДВС', 'Впереди', 1.8),
(9, '2020-10-14 00:00:00', '2020-10-14 00:00:00', 'Хэтчбек', 3, 5, 'ДВС', 'Впереди', 1.8),
(10, '2020-10-14 00:00:00', '2020-10-14 00:00:00', 'Хэтчбек', 3, 5, 'ДВС', 'Впереди', 1.6),
(11, '2020-10-14 00:00:00', '2020-10-14 00:00:00', 'Хэтчбек', 5, 5, 'ДВС', 'Впереди', 2.4);

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL COMMENT 'Идентификатор',
  `create_dt` date NOT NULL DEFAULT curdate() COMMENT 'Дата создания записи',
  `update_dt` datetime NOT NULL DEFAULT curdate() COMMENT 'Дата обновления записи',
  `login` varchar(150) NOT NULL COMMENT 'Логин пользователя для входа в систему',
  `password` varchar(150) NOT NULL COMMENT 'Пароль пользователя для входа в систему',
  `email` varchar(150) NOT NULL COMMENT 'Адрес электронной почты',
  `role_id` int(11) NOT NULL COMMENT 'Идентификатор роли'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Пользователи системы';

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `create_dt`, `update_dt`, `login`, `password`, `email`, `role_id`) VALUES
(1, '2020-10-14', '2020-10-14 00:00:00', 'admin1', 'admin1', 'admin1@mail.ru', 1),
(2, '2020-10-14', '2020-10-14 00:00:00', 'admin2', 'admin2', 'admin2@mail.ru', 1),
(3, '2020-10-14', '2020-10-14 00:00:00', 'admin3', 'admin3', 'admin3@mail.ru', 1),
(4, '2020-10-14', '2020-10-14 00:00:00', 'user1', 'user1', 'user1@mail.ru', 2),
(5, '2020-10-14', '2020-10-14 00:00:00', 'user2', 'user2', 'user2@mail.ru', 2),
(6, '2020-10-14', '2020-10-14 00:00:00', 'user3', 'user3', 'user3@mail.ru', 2),
(7, '2020-10-14', '2020-10-14 00:00:00', 'user4', 'user4', 'user4@mail.ru', 2),
(8, '2020-10-14', '2020-10-14 00:00:00', 'user5', 'user5', 'user5@mail.ru', 2),
(9, '2020-10-14', '2020-10-14 00:00:00', 'user6', 'user6', 'user6@mail.ru', 2),
(10, '2020-10-14', '2020-10-14 00:00:00', 'user7', 'user7', 'user7@mail.ru', 2),
(11, '2020-10-14', '2020-10-14 00:00:00', 'user8', 'user8', 'user8@mail.ru', 2),
(12, '2020-10-14', '2020-10-14 00:00:00', 'user9', 'user9', 'user9@mail.ru', 2),
(13, '2020-10-14', '2020-10-14 00:00:00', 'user10', 'user10', 'user10@mail.ru', 2),
(14, '2020-10-14', '2020-10-14 00:00:00', 'user11', 'user11', 'user11@mail.ru', 2),
(15, '2020-10-14', '2020-10-14 00:00:00', 'user12', 'user12', 'user12@mail.ru', 2);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `basket`
--
ALTER TABLE `basket`
  ADD PRIMARY KEY (`id`),
  ADD KEY `basket_FK_client` (`client_id`),
  ADD KEY `basket_FK_produck` (`product_id`),
  ADD KEY `basket_FK_payment` (`payment_id`),
  ADD KEY `basket_fk_basket_status` (`basket_status_id`);

--
-- Индексы таблицы `basket_status`
--
ALTER TABLE `basket_status`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`id`),
  ADD KEY `brand_fk_country` (`country_id`);

--
-- Индексы таблицы `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_fk_technical_details` (`technical_details_id`),
  ADD KEY `product_fk_brand` (`brand_id`);

--
-- Индексы таблицы `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `technical_details`
--
ALTER TABLE `technical_details`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_fk_role` (`role_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `basket`
--
ALTER TABLE `basket`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор', AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT для таблицы `basket_status`
--
ALTER TABLE `basket_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор', AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `brand`
--
ALTER TABLE `brand`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор', AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT для таблицы `client`
--
ALTER TABLE `client`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор', AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT для таблицы `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор', AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `payment`
--
ALTER TABLE `payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор', AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор', AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT для таблицы `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор', AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `technical_details`
--
ALTER TABLE `technical_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор', AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор', AUTO_INCREMENT=16;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `basket`
--
ALTER TABLE `basket`
  ADD CONSTRAINT `basket_FK_client` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `basket_FK_payment` FOREIGN KEY (`payment_id`) REFERENCES `payment` (`id`),
  ADD CONSTRAINT `basket_FK_produck` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `basket_fk_basket_status` FOREIGN KEY (`basket_status_id`) REFERENCES `basket_status` (`id`);

--
-- Ограничения внешнего ключа таблицы `brand`
--
ALTER TABLE `brand`
  ADD CONSTRAINT `brand_fk_country` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`);

--
-- Ограничения внешнего ключа таблицы `client`
--
ALTER TABLE `client`
  ADD CONSTRAINT `clinet_fk_user` FOREIGN KEY (`id`) REFERENCES `user` (`id`);

--
-- Ограничения внешнего ключа таблицы `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_fk_brand` FOREIGN KEY (`brand_id`) REFERENCES `brand` (`id`),
  ADD CONSTRAINT `product_fk_technical_details` FOREIGN KEY (`technical_details_id`) REFERENCES `technical_details` (`id`);

--
-- Ограничения внешнего ключа таблицы `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_fk_role` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
